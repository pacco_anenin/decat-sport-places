export async function getPositions(ville, rayon, sport) {
  const res = await fetch(
    `http://localhost:3000/overpass/GetAround/${rayon}/${ville[0]}/${ville[1]}/${sport}`,
    {
      mode: "cors",
      method: "get",
      headers: { "Content-Type": "application/json" },
    }
  );
  if (!res.ok) {
    throw new Error(await res.text());
  } else {
    return await res.json();
  }
}

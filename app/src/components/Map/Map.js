import {
  MapContainer,
  TileLayer,
  LayersControl,
  LayerGroup,
} from "react-leaflet";
import { useEffect } from "react";
import PrintMarkers from "./PrintMarkers";
import "./Map.css";
import PrintParams2 from "./PrintParams2";
import usePositions from "../../hooks/usePositions";
import { getPositions } from "../../business/PositionBusiness";
import useCity from "../../hooks/useCity";
import useRayon from "../../hooks/useRayon";
import useSports from "../../hooks/useSports";

export default function Map() {
  // let [showTooltip, setShowTooltip] = useState(false);
  // let mapRef = useRef();
  let { positions, setPositions } = usePositions();
  let { city, setCity } = useCity();
  let { sports, setSports } = useSports();
  let { rayon, setRayon } = useRayon();
  useEffect(() => {
    console.log(positions);
  }, [setPositions, positions, sports, rayon, city]);
  console.log("positions", positions);
  return (
    <div style={{ flex: 3 }}>
      <PrintParams2
        style={{
          height: "100px",
          width: "200px",
          marginBottom: "100px",
          flex: 2,
        }}
        setCity={setCity}
        city={city}
        setRayon={setRayon}
        rayon={rayon}
        sports={sports}
        setSports={setSports}
        setPositions={setPositions}
        positions={positions}
      />
      <br /> <br /> <br /> <br /> <br />
      <MapContainer
        center={[49.310199670257205, 2.239540363785132]}
        zoom={5}
        maxZoom={12000}
        style={{ height: "600px", width: "1000px" }}
      >
        <link
          rel="stylesheet"
          href="https://unpkg.com/leaflet@1.0.1/dist/leaflet.css"
        />
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <LayersControl position="topright">
          <LayersControl.Overlay checked name="Lieux">
            <LayerGroup>
              <PrintMarkers lieux={positions} />
            </LayerGroup>
          </LayersControl.Overlay>
        </LayersControl>
      </MapContainer>
    </div>
  );
}

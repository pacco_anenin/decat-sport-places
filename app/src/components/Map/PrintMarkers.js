import { Marker, Tooltip, Popup } from "react-leaflet";

function TooltipMarker({ position, desc, sport }) {
  return (
    <Marker position={position}>
      <Popup>
        <h1>Name : {desc}</h1>
        <p>Sport : {sport}</p>
      </Popup>
      <Tooltip>
        <h1>Name : {desc}</h1>
        <p>Sport : {sport}</p>
      </Tooltip>
    </Marker>
  );
}

export default function PrintMarkers({ lieux }) {
  console.log("LIEUX");
  console.log(lieux);

  if (lieux == null) {
    return null;
  }

  let markers = [];
  if (lieux.elements) {
    lieux.elements.forEach((lieu) => {
      if (lieu.center !== undefined) {
        markers.push(
          <div key={lieu.id}>
            <TooltipMarker
              position={[lieu.center.lat, lieu.center.lon]}
              desc={lieu.tags.name}
              sport={lieu.tags.sport}
            />
          </div>
        );
      }
    });
  }
  return markers;
}

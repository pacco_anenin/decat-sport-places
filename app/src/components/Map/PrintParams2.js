import { TextField, FormControl, Button } from "@mui/material";
import Select from "react-select";
import { getPositions } from "../../business/PositionBusiness";
import { useEffect } from "react";

//Solution 2 :
// je fais des requêtes à un serveur qui avec le nom d'une ville me renvoie longlatt
// mais plus d'auto complete dans la barre de recherche
// bien plus rapide

const sport_type = [
  { type: "aerobics" },
  { type: "american_football" },
  { type: "badminton" },
  { type: "basketball" },
  { type: "climbing" },
  { type: "fitness" },
  { type: "golf" },
  { type: "horse_racing" },
];

// const searchTownPoints = function (e) {
//   if (e.code === "Enter" || e.code === "NumpadEnter") {
//     console.log("ENTER PRESS");
//     var ville = document.getElementById("maVillePref").value;
//     console.log(ville);
//   }
// };

const requestServer = async function (city, rayon, sports) {
  console.log("REQUETE");

  if (city !== "") {
    const req =
      "https://geo.api.gouv.fr/communes?nom=" +
      city +
      "&fields=nom,code,codesPostaux,centre&format=json&geometry=centre";

    console.log(req);
    let lat,
      long,
      ray,
      sportsStr = "";

    ray = rayon !== "" ? rayon : "20";

    sportsStr = sports.join(",");

    const res1 = await fetch(req);
    const res = await res1.json();

    lat = res[0].centre.coordinates[1];
    long = res[0].centre.coordinates[0];
    const pos = getPositions([lat, long], ray, sports);
    console.log("pos", pos);
    return pos;
  }
};

export default function PrintParams({
  setCity,
  city,
  setRayon,
  rayon,
  sports,
  setSports,
  setPositions,
  positions,
}) {
  return (
    <div
      className="App"
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "space-around",
        marginTop: 30,
      }}
    >
      <TextField
        id="maVillePref"
        label={"ville"}
        onChange={(e) => {
          setCity(e.target.value);
        }}
      />

      <TextField
        type={"number"}
        label={"rayon"}
        InputProps={{ inputProps: { min: 1, max: 100 } }}
        onChange={(e) => {
          setRayon(e.target.value);
        }}
      />

      <FormControl style={{ minWidth: 400, lineHeight: 2.5 }}>
        <Select
          getOptionValue={(option) => option.type}
          getOptionLabel={(option) => option.type}
          options={sport_type}
          isMulti
          placeholder="Sports type"
          onChange={(e) => {
            let mesSports = [];
            for (const i of e) {
              mesSports.push(i.type);
            }
            setSports(mesSports);
          }}
        />
        <br />
      </FormControl>

      <Button
        variant="outlined
        "
        onClick={(e) => {
          const fetchData = async () => {
            try {
              const result = await requestServer(city, rayon, sports);
              console.log("testored");
              console.log(result);
              setPositions(result);
            } catch (error) {
              console.error(
                "Une erreur s'est produite lors de la récupération des positions :",
                error
              );
            }
          };
          fetchData();
        }}
      >
        Search
      </Button>
    </div>
  );
}

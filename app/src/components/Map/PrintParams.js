import Select from "react-select";
import cities from "../../cities-data/cities.json";

//Solution 1 :
// j'ai un fichier JSON local, avec toutes les villes de France et leur longlat
// mais le chargement du select est tres long je trouve

const searchTownPoints = function (val) {};

export default function PrintParams() {
  console.log(cities);

  return (
    <div className="App">
      <h1>Find your spot :!</h1>
      <Select
        getOptionValue={(option) => option.insee_code}
        getOptionLabel={(option) => option.city_code}
        options={cities["cities"]}
        onChange={(e) => {
          searchTownPoints(e);
        }}
      />
    </div>
  );
}

import "./index.css";
import Map from "../Map/Map";
import PositionContextProvider from "../../contexts/PositionContext";

import { createBrowserRouter, RouterProvider } from "react-router-dom";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Map />,
  },
]);

function App() {
  return (
    <PositionContextProvider>
      <div className="App">
        <header className="App-header">
          <RouterProvider router={router} />
        </header>
      </div>
    </PositionContextProvider>
  );
}

export default App;

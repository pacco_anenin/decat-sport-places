import { useState } from "react";

export default function useRayon() {
  const [rayon, setRayon] = useState("");
  return { rayon, setRayon };
}

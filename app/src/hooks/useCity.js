import { useState } from "react";

export default function useCity() {
  const [city, setCity] = useState("");
  return { city, setCity };
}

import { useContext } from "react";

import { PositionContext } from "../contexts/PositionContext";

export default function usePositions() {
  const { positions, setPositions } = useContext(PositionContext);
  return { positions, setPositions };
}

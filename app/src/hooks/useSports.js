import { useState } from "react";

export default function useSports() {
  const [sports, setSports] = useState("");
  return { sports, setSports };
}

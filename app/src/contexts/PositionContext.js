import { useState, createContext } from "react";

export const PositionContext = createContext();

export default function PositionContextProvider({ children }) {
  let [positions, setPositions] = useState([]);

  return (
    <PositionContext.Provider value={{ positions, setPositions }}>
      {children}
    </PositionContext.Provider>
  );
}

import { Request, Response, NextFunction } from "express";
const cors = require("cors");

const express = require("express"); // inclusion d'express

// Instanciation d'une application express
const myApp = express();

const corsOptions = {
  origin: "http://localhost:3001", // Remplacez par votre frontend URL
  methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
  credentials: true,
  optionsSuccessStatus: 204,
};
myApp.use(express.json());

// Configuration de l'application : une première gestion "basique" des requêtes.
myApp.use((req: Request, res: Response, next: NextFunction) => {
  // Une fois encore, les requêtes sont pour l'instant toutes traitées de la même manière.
  const now = new Date().toDateString();
  console.log(`${now} : une requête ${req.method} est arrivée !`);
  next();
});

// --- ROUTAGE ---
//pour décodage des requêtes POST :
// app.use(express.json()) ;
// app.use(express.urlencoded({extended : false})) ;

myApp.use(cors(corsOptions));

const homeRouter = require("./routes/homeRouter");
myApp.use("/", homeRouter);

const overpassRouter = require("./routes/overpassRouter");
myApp.use("/overpass", overpassRouter);

import commentRouter from "./routes/commentRouter";
myApp.use("/comment", commentRouter);

// Exportation de notre application express
module.exports = myApp;
import {config} from "dotenv";
config({ path: "./.env" });

import { PrismaClient } from "@prisma/client";

export const prisma = new PrismaClient();

async function main() {
  const port = process.env.PORT || 3000;
  const http = require("http");
  const app = require("./app");
  const server = http.createServer(app);

  if (!process.env.TEST_ENV)
    server.listen(port, () => {
      console.log(`Le server écoute sur http://127.0.0.1:${port}/`);
    });
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async e => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });

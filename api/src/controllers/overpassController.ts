import { Request, Response, NextFunction } from "express";

/**
 * Retrieves places from the Overpass API
 *
 * @param {Request} req - The request object containing the parameters for the API call.
 * @param {Response} res - The response object used to send the JSON response.
 * @return {void} This function does not return anything.
 */
module.exports.getAround = async (req: Request, res: Response) => {
  let overpass_url: string = "https://overpass-api.de/api/interpreter";
  let query = createQuery(req);
  const api = await fetch(overpass_url, {
    method: "POST",
    body: query,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
  });
  const response = await api.json();
  let json = formatJson(response);
  res.json(json);
};

/**
 * Removes unnecessary properties from a JSON object.
 *
 * @param {any} json - The JSON object to be formatted.
 * @return {any} The formatted JSON object.
 */
function formatJson(json: any) {
  delete json.version;
  delete json.generator;
  delete json.osm3s;
  delete json.copyright;

  let properties = ["id", "center", "tags"];
  for (let i in json.elements) {
    for (let property in json.elements[i]) {
      if (!properties.includes(property)) {
        delete json.elements[i][property];
      }
    }
  }

  return json;
}

/**
 * Generates a query string to be sent to the Overpass API.
 *
 * @param {Request} req - The request object containing the parameters for the query.
 * @return {string} The query string.
 */
function createQuery(req: Request) {
  let leisures = [
    "fitness_centre",
    "sports_centre",
    "sports_hall",
    "stadium",
    "track",
    "pitch",
    "horse_riding",
    "swimming_pool",
    "golf_course",
    "ice_rink",
    "fishing",
  ];

  if (req.params.sports) {
    req.params.sports = req.params.sports.replace(",", "|");
  }

  let result = `[out:json];
    (
        nwr(around:${req.params.radius}, ${req.params.longitude}, ${req.params.latitude})["brand"~"Decathlon"];
        nwr(around:${req.params.radius}, ${req.params.longitude}, ${req.params.latitude})["landuse"="winter_sports"];`;

  for (let leisure of leisures) {
    if (req.params.sports) {
      result += `nwr(around:${req.params.radius}, ${req.params.longitude}, ${req.params.latitude})
            ["sport"~"${req.params.sports}"];`;
    } else {
      result += `nwr(around:${req.params.radius}, ${req.params.longitude}, ${req.params.latitude})
            ["leisure"="${leisure}"];`;
    }
  }

  result += `);
    out center;`;

  return result;
}

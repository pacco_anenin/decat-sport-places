import { prisma } from "../index";
import { Request, Response } from "express";

export const getAllCommentsFromId = async (placeId: string) => {
  return prisma.comment.findMany({ where: { placeId: placeId } });
};

/**
 * GET Route : /comments/avg/:placeId
 */
export const getRatingsAvgFromId = async (req: Request, res: Response) => {
  const placeId: string | undefined = req.params.placeId;

  if (typeof placeId === "undefined") {
    return res.status(400).json({
      code: "NO_PLACE_ID_PARAM",
      error: "Please provide placeId in requests parameters",
    });
  }

  const aggregations = await prisma.comment.aggregate({
    _avg: {
      rating: true,
    },
    where: {
      placeId,
    },
  });

  const ratingAvg = aggregations._avg.rating;

  if (ratingAvg == null) {
    return res.json({
      avg: null,
    });
  }

  return res.json({ avg: parseFloat(ratingAvg.toFixed(1)) });
};

/**
 * POST Route : /comments/add
 * Body : { email: string, content: string?, rating: int, placeId: string }
 */
export async function addNewComment(
  req: Request,
  res: Response,
): Promise<void> {
  const count = await prisma.comment.count({
    where: { email: req.body.email, placeId: req.body.placeId },
  });

  // if a comment with the same mail already exists
  if (count !== 0) {
    res.status(400).json({
      code: "COMMENT_EMAIL_ALREADY_USED",
      error: "Email already used to put a comment",
    });
    return;
  }

  res.status(201).json(
    await prisma.comment.create({
      data: {
        email: req.body.email,
        placeId: req.body.placeId,
        rating: req.body.rating,
        content: req.body.content,
      },
    }),
  );
}

export async function removeAllCommentForId(
    req: Request,
    res: Response,
) {
  await prisma.comment.deleteMany({where: { placeId: req.params.id }});
  return res.status(410).send();
}
var express = require('express')
var router = express.Router()

const overpassController = require("../controllers/overpassController") ;

router.get('/GetAround/:radius/:longitude/:latitude/:sports?', overpassController.getAround);

module.exports = router;    
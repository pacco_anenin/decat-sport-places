import express, {Request, Response} from "express";
import {
    getAllCommentsFromId,
    addNewComment,
    removeAllCommentForId,
    getRatingsAvgFromId,
} from "../controllers/commentController";

const router = express.Router();

router.get("/:placeId", async (req: Request, res: Response) => {
  const placeId = req.params.placeId;
  try {
    const comments = await getAllCommentsFromId(placeId);
    res.status(200).json(comments);
  } catch (err) {
    console.error(err);
  }
});

router.get("/avg/:placeId", (req, res) => {
  return getRatingsAvgFromId(req, res);
});

router.post("/", (req, res) => {
  return addNewComment(req, res);
});

router.delete("/:id", (req, res) => {
    return removeAllCommentForId(req, res);
});

export default router;

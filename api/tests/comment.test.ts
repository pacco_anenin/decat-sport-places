import supertest from "supertest";
import { Comment } from "@prisma/client";
const app = require("../src/app");

describe("Tests with comments", () => {

    it('Try delete all comments from id', async () => {
        return supertest(app)
            .delete("/comment/0")
            .expect(410);
    });

    it('Check if empty list of comment', async () => {
        return supertest(app)
            .get("/comment/0")
            .expect("Content-Type", /json/)
            .expect(200)
            .then(res => {
                expect(res.body).toStrictEqual([]);
            });
    });

    it('Add a valid comment', async () => {
        return supertest(app)
            .post("/comment/")
            .send({
                email: "toto@foo.bar",
                placeId: "0",
                rating: 3,
                content: "je préfère Norman",
            })
            .expect(201);
    });

    it('Check if list of comment not empty', async () => {
        return supertest(app)
            .get("/comment/0")
            .expect("Content-Type", /json/)
            .expect(200)
            .then(res => {
                expect(res.body).not.toStrictEqual([]);
            });
    });

});
